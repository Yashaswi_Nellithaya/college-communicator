package com.yashaswi.android.citcommunicator;

/**
 * Created by yashaswi on 17/05/17.
 */

public class Constants {

    public final static String HOME_FRAGMENT = "HOME_FRAGMENT";
    public static final String CHOOSE_BRANCH_FRAGMENT = "CHOOSE_BRANCH_FRAGMENT";

    public static final String ADMIN_FRAGMENT = "ADMIN_FRAGMENT";
    public static final String FACULTY_FRAGMENT = "FACULTY_FRAGMENT";
    public static final String FACULTY_LIST = "FACULTY_LIST";
    public static final String STUDENT_LIST = "STUDENT_LIST";
    public static final String ABOUT_FRAGMENT = "ABOUT_FRAGMENT";
    public static final String FACULTY_DETAIL_FRAGMENT = "FACULTY_DETAIL_FRAGMENT";
}
