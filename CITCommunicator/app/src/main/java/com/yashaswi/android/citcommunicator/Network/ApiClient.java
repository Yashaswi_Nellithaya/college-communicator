package com.yashaswi.android.citcommunicator.Network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by yashaswi on 30/05/17.
 */

public class ApiClient {
    private static String BASE_URL="http://api.nktworks.com/api/1.0/";
    private static Retrofit retrofit=null;

    public static Retrofit getClient()
    {
         retrofit=new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

}
