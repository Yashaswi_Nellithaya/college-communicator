package com.yashaswi.android.citcommunicator.Network;

import com.google.gson.JsonObject;
import com.yashaswi.android.citcommunicator.jsonmodels.FacultyModel;
import com.yashaswi.android.citcommunicator.jsonmodels.StudentModel;
import com.yashaswi.android.citcommunicator.jsonmodels.UserModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by yashaswi on 30/05/17.
 */

public interface ApiInterface {
    @GET("users")
    Call<UserModel> getUserData();
    @POST("users")
    Call<UserModel> postUserData(@Body JsonObject jsonObject);
    @GET("users/{user_id}")
    Call<UserModel> getUserIdData(@Path("user_id") String userId);
    @DELETE("users/{user_id}")
    Call<UserModel> deteleUserIdDate(@Path("user_id") String userId);
    @POST("faculty")
    Call<FacultyModel> addFaculty(@Body JsonObject jsonObject);
    @GET("faculty")
    Call<FacultyModel> getFcaultyData();
    @POST("students")
    Call<StudentModel> addStudent(@Body JsonObject jsonObject);
    @GET("students")
    Call<StudentModel> getStudentData();


}
