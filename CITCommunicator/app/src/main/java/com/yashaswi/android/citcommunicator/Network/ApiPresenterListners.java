package com.yashaswi.android.citcommunicator.Network;

import com.yashaswi.android.citcommunicator.jsonmodels.ShareModel;

import retrofit2.Call;

/**
 * Created by yashaswi on 30/05/17.
 */

public interface ApiPresenterListners {
    public void showError(Exception e);
    public void showResult(Call<ShareModel> shareModelCall);

}
