package com.yashaswi.android.citcommunicator.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.yashaswi.android.citcommunicator.Constants;
import com.yashaswi.android.citcommunicator.R;
import com.yashaswi.android.citcommunicator.fragments.AboutFragment;
import com.yashaswi.android.citcommunicator.fragments.ChooseBranchFragment;

public class HomeMainActivity extends AppCompatActivity {

    public static ImageView moreBtn;
    public static TextView toolBarTitle;
    public  static Toolbar mainToolBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_main);
        moreBtn = (ImageView) findViewById(R.id.moreBtn);
        toolBarTitle = (TextView) findViewById(R.id.titleTV);
        mainToolBar=(Toolbar)findViewById(R.id.mainToolBar);
        moreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMoreDialog();
                broadcastSender(v);
            }
        });
        setDefaultFragment();

//        databaseHandler = new DatabaseHandler(HomeMainActivity.this);
//        if (databaseHandler != null) {
//            Log.i("db name", databaseHandler.getDatabaseName());
//            Log.i("Environ data directory", Environment.getDataDirectory().toString());
//        }
//        databaseHandler.addStudent(new StudentDataModel(1,"Yashaswi","CSE","8th",80,100,R.drawable.admin_picture));
//        databaseHandler.addStudent(new StudentDataModel(2,"Yashaswi","CSE","8th",80,100,R.drawable.admin_picture));
//        databaseHandler.addStudent(new StudentDataModel(3,"Yashaswi","CSE","8th",80,100,R.drawable.admin_picture));
//        databaseHandler.deleteContact(1);
    }

    private void broadcastSender(View view) {
        Intent intent = new Intent();
        intent.setAction("com.cit.CUSTOM_INTENT");
        sendBroadcast(intent);
    }

    public static void setTitle(String title) {
        toolBarTitle.setText(title);
    }

    public static void hideMoreButton() {
        moreBtn.setVisibility(View.GONE);
    }

    public static void showMoreButton() {
        moreBtn.setVisibility(View.VISIBLE);
    }

    private void setMoreDialog() {
        CharSequence[] items = new CharSequence[]{"About the App", "Send Feedback"};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        AboutFragment aboutFragment = AboutFragment.newInstance();
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.fragment_container, aboutFragment, Constants.ABOUT_FRAGMENT);
                        fragmentTransaction.addToBackStack(Constants.HOME_FRAGMENT);
                        fragmentTransaction.commit();
                        break;
                    case 1:
                        dialog.dismiss();
                        setFeedbackDialog();
                        break;
                }
            }
        }).show();

    }

    private void setFeedbackDialog() {
        LayoutInflater li = LayoutInflater.from(this);
        View customView = li.inflate(R.layout.feedback_view, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(customView);
        final EditText userInput = (EditText) customView.findViewById(R.id.msg);
        userInput.setHint(" Please tell us your feedback");
        alertDialogBuilder
                .setTitle("Feedback")
                .setCancelable(true)
                .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!TextUtils.isEmpty(userInput.getText())) {
                            sendMail(userInput.getText().toString());
                        } else {
                            Toast.makeText(HomeMainActivity.this, "Please enter the feedback text", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();

    }

    private void sendMail(String text) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"nnnaveen.nag@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "Feedback!");
        i.putExtra(Intent.EXTRA_TEXT, text);
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }


    private void setDefaultFragment() {
        ChooseBranchFragment chooseBranchFragment = ChooseBranchFragment.newInstance();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container, chooseBranchFragment, Constants.CHOOSE_BRANCH_FRAGMENT);
        fragmentTransaction.commit();
    }



}
