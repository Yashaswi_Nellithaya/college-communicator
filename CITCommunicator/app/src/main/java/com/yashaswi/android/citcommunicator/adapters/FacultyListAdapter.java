package com.yashaswi.android.citcommunicator.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yashaswi.android.citcommunicator.R;
import com.yashaswi.android.citcommunicator.jsonmodels.FacultyDataModel;

/**
 * Created by yashaswi on 15/06/17.
 */

public class FacultyListAdapter extends BaseAdapter {


    private final Context context;
    private final FacultyDataModel[] facultyData;
    TextView mNameTV;
    TextView mBranchTV;
    TextView mSemTV;
    TextView mAttendanceTv;
    TextView mMarksTV;
    ImageView mMoredownBtn;
    ImageView profilePicture;
    private Integer[] profileImag = {R.drawable.feed_person, R.drawable.img_feed_person, R.drawable.feed_person, R.drawable.img_img_feed, R.drawable.img_img_person, R.drawable.feed_person, R.drawable.img_img_person, R.drawable.img_img_person, R.drawable.feed_person, R.drawable.img_img_person_icon, R.drawable.img_img_person_icon, R.drawable.feed_person, R.drawable.feed_person, R.drawable.feed_person, R.drawable.img_img_person_icon, R.drawable.feed_person};


    public FacultyListAdapter(Context context, FacultyDataModel[] facultyDataModel) {
        this.context = context;
        facultyData = facultyDataModel;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
        mNameTV = (TextView) view.findViewById(R.id.nameText);
        mBranchTV = (TextView) view.findViewById(R.id.branchTV);
        mSemTV = (TextView) view.findViewById(R.id.semTV);
        mAttendanceTv = (TextView) view.findViewById(R.id.attendanceTV);
        mMarksTV = (TextView) view.findViewById(R.id.marksTV);
        mMoredownBtn = (ImageView) view.findViewById(R.id.downArrow);
        profilePicture = (ImageView) view.findViewById(R.id.profileIV);
        updateView(position);
        return view;
    }

    private void updateView(int position) {
        if(facultyData!=null && facultyData.length>0) {
            FacultyDataModel tempData=facultyData[position];
            profilePicture.setImageResource(profileImag[position]);
            mNameTV.setText(tempData.getName());
            mBranchTV.setVisibility(View.GONE);
            mSemTV.setVisibility(View.GONE);
            mAttendanceTv.setVisibility(View.GONE);
            mMarksTV.setVisibility(View.GONE);
            mMoredownBtn.setVisibility(View.GONE);
        }
    }

    @Override
    public int getCount() {
        return facultyData.length;
    }

    @Override
    public FacultyDataModel getItem(int position) {
        return facultyData[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
