package com.yashaswi.android.citcommunicator.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.yashaswi.android.citcommunicator.R;
import com.yashaswi.android.citcommunicator.jsonmodels.StudentDataModel;

/**
 * Created by yashaswi on 15/06/17.
 */

public class StudentListAdapter extends BaseAdapter {


    private final Context context;
    private final StudentDataModel[] studentData;
    TextView mNameTV;
    TextView mBranchSemTV;
    TextView mAttendanceTv;
    TextView mMarksTV;
    ImageView mMoredownBtn;
    ImageView profilePicture;
    private Integer[] profileImag = {R.drawable.feed_person, R.drawable.img_feed_person, R.drawable.feed_person, R.drawable.img_img_feed, R.drawable.img_img_person, R.drawable.feed_person, R.drawable.img_img_person, R.drawable.img_img_person, R.drawable.feed_person, R.drawable.img_img_person_icon, R.drawable.img_img_person_icon, R.drawable.feed_person, R.drawable.feed_person, R.drawable.feed_person, R.drawable.img_img_person_icon, R.drawable.feed_person};
    private TextView mSemTV;


    public StudentListAdapter(Context context, StudentDataModel[] facultyDataModel) {
        this.context = context;
        studentData = facultyDataModel;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
        mNameTV = (TextView) view.findViewById(R.id.nameText);
        mBranchSemTV = (TextView) view.findViewById(R.id.branchTV);
        mAttendanceTv = (TextView) view.findViewById(R.id.attendanceTV);
        mMarksTV = (TextView) view.findViewById(R.id.marksTV);
        mSemTV = (TextView) view.findViewById(R.id.semTV);
        mMoredownBtn = (ImageView) view.findViewById(R.id.downArrow);
        profilePicture = (ImageView) view.findViewById(R.id.profileIV);
        updateView(position);
        return view;
    }

    private void updateView(int position) {
        if (studentData != null && studentData.length > 0) {
            final StudentDataModel tempData = studentData[position];
            profilePicture.setVisibility(View.GONE);
            mNameTV.setText(tempData.getName());
            mNameTV.setTypeface(Typeface.DEFAULT_BOLD);
            mBranchSemTV.setText(tempData.getDept());
            mSemTV.setText("Semester : " + tempData.getSem());
            mAttendanceTv.setText("Attendance : " + tempData.getAttendance() + "/100");
            mMarksTV.setText("Total internal marks : " + tempData.getMarks());
            mMoredownBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    LayoutInflater li = LayoutInflater.from(context);
                    View customView = li.inflate(R.layout.feedback_view, null);

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setView(customView);
                    final EditText userInput = (EditText) customView.findViewById(R.id.msg);

                    userInput.setHint("Enter Parent's Phone Number");
                    alertDialogBuilder
                            .setTitle("SMS Alert!")
                            .setCancelable(true)
                            .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (!TextUtils.isEmpty(userInput.getText())) {
                                        sendSMS(userInput.getText().toString(), tempData.getAttendance(), tempData.getMarks());
                                    } else {
                                        Toast.makeText(context, "Please enter the Phone Number", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            });
        }
    }

    private void sendSMS(String s, int attendance, int marks) {
        Log.i("Send SMS", "");
        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        smsIntent.setData(Uri.parse("smsto:"));
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address", new String(s));
        smsIntent.putExtra("sms_body", "Attendance : " + attendance + ", Marks : " + marks);

        try {
            context.startActivity(smsIntent);
            Log.i("Finished sending SMS...", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context,
                    "SMS faild, please try again later.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public int getCount() {
        return studentData.length;
    }

    @Override
    public StudentDataModel getItem(int position) {
        return studentData[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
