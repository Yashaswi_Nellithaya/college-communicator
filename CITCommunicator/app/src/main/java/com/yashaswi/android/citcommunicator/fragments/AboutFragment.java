package com.yashaswi.android.citcommunicator.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yashaswi.android.citcommunicator.Network.ApiClient;
import com.yashaswi.android.citcommunicator.Network.ApiInterface;
import com.yashaswi.android.citcommunicator.R;
import com.yashaswi.android.citcommunicator.activities.HomeMainActivity;
import com.yashaswi.android.citcommunicator.jsonmodels.UserModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yashaswi on 23/05/17.
 */

public class AboutFragment extends Fragment {


    public static AboutFragment newInstance() {
        Bundle args = new Bundle();
        AboutFragment fragment = new AboutFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.from(getActivity()).inflate(R.layout.about_app, container, false);
        HomeMainActivity.hideMoreButton();
        HomeMainActivity.setTitle("About App!");
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<UserModel> shareModelCall = apiInterface.getUserIdData("5937008539e66d36e8c5e89b");
        shareModelCall.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                UserModel shareModel = response.body();

             //   Toast.makeText(getActivity(), "Data length : " + shareModel.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                t.printStackTrace();
            }
        });
        return view;
    }
}
