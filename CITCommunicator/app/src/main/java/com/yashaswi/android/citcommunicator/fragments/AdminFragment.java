package com.yashaswi.android.citcommunicator.fragments;

import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.yashaswi.android.citcommunicator.Network.ApiInterface;
import com.yashaswi.android.citcommunicator.R;
import com.yashaswi.android.citcommunicator.activities.HomeMainActivity;

import java.util.ArrayList;

/**
 * Created by yashaswi on 17/05/17.
 */

public class AdminFragment extends Fragment {

    private ArrayList<String> facultyNames;
    private ApiInterface apiInterface;

    public static AdminFragment newInstance() {
        Bundle args = new Bundle();
        AdminFragment fragment = new AdminFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.from(getActivity()).inflate(R.layout.admin, container, false);
        HomeMainActivity.setTitle("Admin");
        HomeMainActivity.hideMoreButton();
        Window window = getActivity().getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getActivity(),R.color.lightOrange));
        }

        return view;
    }

}
