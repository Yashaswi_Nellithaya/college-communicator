package com.yashaswi.android.citcommunicator.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.yashaswi.android.citcommunicator.Constants;
import com.yashaswi.android.citcommunicator.R;
import com.yashaswi.android.citcommunicator.activities.HomeMainActivity;

/**
 * Created by yashaswi on 16/05/17.
 */

public class ChooseBranchFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private Spinner mySpinner;
    private Button submit;
    private String itemSelected;

    public static ChooseBranchFragment newInstance() {
        ChooseBranchFragment chooseBranchFragment = new ChooseBranchFragment();
        return chooseBranchFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.from(getActivity()).inflate(R.layout.choose_branch, container, false);
        HomeMainActivity.setTitle("College Communicator");
        HomeMainActivity.showMoreButton();
        mySpinner = (Spinner) view.findViewById(R.id.spinner);
        submit = (Button) view.findViewById(R.id.button);
        HomeMainActivity.mainToolBar.setVisibility(View.GONE);
        mySpinner.setOnItemSelectedListener(this);

        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.names));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeFragment homeFrament = HomeFragment.newInstance(itemSelected);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, homeFrament, Constants.HOME_FRAGMENT);
                fragmentTransaction.addToBackStack(Constants.CHOOSE_BRANCH_FRAGMENT);
                fragmentTransaction.commit();
             //   Toast.makeText(getActivity(), "Branch Selected : "+itemSelected, Toast.LENGTH_SHORT).show();

            }
        });

        mySpinner.setAdapter(myAdapter);
        return view;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        itemSelected = parent.getItemAtPosition(position).toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
