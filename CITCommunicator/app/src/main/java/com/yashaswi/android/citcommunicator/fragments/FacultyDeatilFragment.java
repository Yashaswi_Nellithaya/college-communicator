package com.yashaswi.android.citcommunicator.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yashaswi.android.citcommunicator.R;
import com.yashaswi.android.citcommunicator.activities.HomeMainActivity;
import com.yashaswi.android.citcommunicator.jsonmodels.FacultyDataModel;

/**
 * Created by yashaswi on 23/05/17.
 */

public class FacultyDeatilFragment extends Fragment {
    private String listing_type = "";
    private TextView deatilTitle;
    private TextView mDept;
    private TextView mSpecialisation;
    private TextView mExperience;
    private TextView mEmail;
    private TextView mPhone;
    private ImageView profilePic;
    private TextView mDesignation;

    FacultyDataModel detailData;

    public static FacultyDeatilFragment newInstance(FacultyDataModel data) {
        Bundle args = new Bundle();
        FacultyDeatilFragment fragment = new FacultyDeatilFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.from(getActivity()).inflate(R.layout.faculty_detail, container, false);
        HomeMainActivity.showMoreButton();
        profilePic = (ImageView) view.findViewById(R.id.facultyprofilePic);
        deatilTitle = (TextView) view.findViewById(R.id.detailTitle);
        mDept = (TextView) view.findViewById(R.id.facDept);
        mSpecialisation = (TextView) view.findViewById(R.id.facSpeci);
        mExperience = (TextView) view.findViewById(R.id.facExp);
        mEmail = (TextView) view.findViewById(R.id.facEmail);
        mPhone = (TextView) view.findViewById(R.id.facPhone);
        mDesignation = (TextView) view.findViewById(R.id.designationTV);
        updateView();
        return view;
    }

    private void updateView() {
        if (detailData != null) {
            deatilTitle.setText(detailData.getName());
            mDesignation.setText(detailData.getDesignation());
            mDept.setText("Department : " + detailData.getDept());
            mSpecialisation.setText("Specialization : " + detailData.getSpecialization());
            mExperience.setText("Year of Experience : " + detailData.getExp());
            mEmail.setText("Email : " + detailData.getEmail());
            mPhone.setText("Phone : " + detailData.getPhn());

        }

    }

    public void setFacultyDetail(FacultyDataModel facultyDataModel) {
        this.detailData = facultyDataModel;
    }
}
