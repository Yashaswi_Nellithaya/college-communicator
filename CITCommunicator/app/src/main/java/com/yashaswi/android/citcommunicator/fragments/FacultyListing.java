package com.yashaswi.android.citcommunicator.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.yashaswi.android.citcommunicator.Constants;
import com.yashaswi.android.citcommunicator.Network.ApiClient;
import com.yashaswi.android.citcommunicator.Network.ApiInterface;
import com.yashaswi.android.citcommunicator.R;
import com.yashaswi.android.citcommunicator.activities.HomeMainActivity;
import com.yashaswi.android.citcommunicator.adapters.FacultyListAdapter;
import com.yashaswi.android.citcommunicator.jsonmodels.FacultyModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yashaswi on 14/06/17.
 */

public class FacultyListing extends Fragment {


    private static final String BRANCH_NAME = "BRANCH_NAME";
    private static final String LISTING_TYPE="LISTING_TYPE";
    private ListView facultyList;
    private FacultyListAdapter listingAdapter;
    private String listingType = "";
    private Integer[] profileImag = {R.drawable.feed_person, R.drawable.img_feed_person, R.drawable.feed_person,R.drawable.img_img_feed, R.drawable.img_img_person,R.drawable.feed_person, R.drawable.img_img_person,R.drawable.img_img_person,R.drawable.feed_person,R.drawable.img_img_person_icon,R.drawable.img_img_person_icon,R.drawable.feed_person,R.drawable.feed_person, R.drawable.feed_person,R.drawable.img_img_person_icon,R.drawable.feed_person};
    private String branchName;
    FacultyModel facultyModel;
    public static FacultyListing newInstance(String listingType, String branchName) {

        Bundle args = new Bundle();
        FacultyListing fragment = new FacultyListing();
        fragment.setArguments(args);
        args.putString(BRANCH_NAME,branchName);
        args.putString(LISTING_TYPE,listingType);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.from(
                getActivity()).inflate(R.layout.faculy_list, container, false);
        HomeMainActivity.hideMoreButton();
        HomeMainActivity.setTitle("Faculties");
        facultyList = (ListView) view.findViewById(R.id.listLV);
        if (getArguments() != null) {
            this.branchName = getArguments().getString(BRANCH_NAME);
            this.listingType=getArguments().getString(LISTING_TYPE);
        }
        if(facultyModel==null) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<FacultyModel> shareModelCall = apiInterface.getFcaultyData();
            shareModelCall.enqueue(new Callback<FacultyModel>() {
                @Override
                public void onResponse(Call<FacultyModel> call, Response<FacultyModel> response) {
                    facultyModel = response.body();
                    showFacultyList(facultyModel);
                    //Toast.makeText(getActivity(), "Data length : " + facultyModel.getData().toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<FacultyModel> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
        else
        {
            showFacultyList(facultyModel);
        }
        return view;
    }

    private void showFacultyList(final FacultyModel data) {
        if(data!=null) {
            listingAdapter = new FacultyListAdapter(getActivity(), data.getData());
            facultyList.setAdapter(listingAdapter);
            facultyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    FacultyDeatilFragment facultyDeatilFragment = FacultyDeatilFragment.newInstance(data.getData()[position]);
                    facultyDeatilFragment.setFacultyDetail(data.getData()[position]);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_container, facultyDeatilFragment, Constants.FACULTY_DETAIL_FRAGMENT);
                    fragmentTransaction.addToBackStack(Constants.FACULTY_FRAGMENT);
                    fragmentTransaction.commit();
                }
            });
        }
    }

}
