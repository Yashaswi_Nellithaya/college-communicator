package com.yashaswi.android.citcommunicator.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.yashaswi.android.citcommunicator.Constants;
import com.yashaswi.android.citcommunicator.R;
import com.yashaswi.android.citcommunicator.activities.HomeMainActivity;

/**
 * Created by yashaswi on 16/05/17.
 */

public class HomeFragment extends Fragment implements View.OnClickListener {

    private static String branchName;
    private static String BRANCH_NAME="BRANCH_NAME";
    private Button adminBtn;
    private Button facultyBtn;
    private Button studentBtn;

    public static HomeFragment newInstance(String itemSelected) {
        Bundle bundle=new Bundle();
        HomeFragment homeFragment = new HomeFragment();
        homeFragment.setArguments(bundle);
        bundle.putString(BRANCH_NAME,itemSelected);
        return homeFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.from(getActivity()).inflate(R.layout.home_fragment, container, false);
        HomeMainActivity.setTitle("College Communicator");
        if(getArguments()!=null)
        {
            branchName=getArguments().getString(BRANCH_NAME);
            Log.i("Branch name", branchName);
        }
        HomeMainActivity.showMoreButton();
        HomeMainActivity.mainToolBar.setVisibility(View.VISIBLE);
        HomeMainActivity.setTitle("Home");
        adminBtn = (Button) view.findViewById(R.id.adminBtn);
        facultyBtn = (Button) view.findViewById(R.id.facultyBtn);
        studentBtn = (Button) view.findViewById(R.id.studentBtn);
        adminBtn.setOnClickListener(this);
        facultyBtn.setOnClickListener(this);
        studentBtn.setOnClickListener(this);
        return view;
    }

//    private void broadcastSender(View view) {
//        Intent intent = new Intent();
//        intent.setAction("com.cit.CUSTOM_INTENT_2");
//        getActivity().sendBroadcast(intent);
//    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.adminBtn:
               //
                // broadcastSender(view);
                AdminFragment adminFragment = AdminFragment.newInstance();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, adminFragment, Constants.ADMIN_FRAGMENT);
                fragmentTransaction.addToBackStack(Constants.HOME_FRAGMENT);
                fragmentTransaction.commit();
                break;
            case R.id.facultyBtn:
                FacultyListing listFragment = FacultyListing.newInstance(Constants.FACULTY_LIST,branchName);
                FragmentManager fragmentManager1 = getFragmentManager();
                FragmentTransaction fragmentTransaction1 = fragmentManager1.beginTransaction();
                fragmentTransaction1.replace(R.id.fragment_container, listFragment, Constants.FACULTY_FRAGMENT);
                fragmentTransaction1.addToBackStack(Constants.HOME_FRAGMENT);
                fragmentTransaction1.commit();
                break;
            case R.id.studentBtn:
                StudentListing studentListing = StudentListing.newInstance(Constants.STUDENT_LIST,branchName);
                FragmentManager fragmentManager2 = getFragmentManager();
                FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                fragmentTransaction2.replace(R.id.fragment_container, studentListing, Constants.FACULTY_FRAGMENT);
                fragmentTransaction2.addToBackStack(Constants.HOME_FRAGMENT);
                fragmentTransaction2.commit();
                break;

        }
    }
}
