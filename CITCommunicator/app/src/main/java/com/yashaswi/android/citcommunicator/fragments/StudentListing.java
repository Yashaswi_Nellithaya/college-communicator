package com.yashaswi.android.citcommunicator.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.yashaswi.android.citcommunicator.Network.ApiClient;
import com.yashaswi.android.citcommunicator.Network.ApiInterface;
import com.yashaswi.android.citcommunicator.R;
import com.yashaswi.android.citcommunicator.activities.HomeMainActivity;
import com.yashaswi.android.citcommunicator.adapters.StudentListAdapter;
import com.yashaswi.android.citcommunicator.jsonmodels.StudentModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yashaswi on 14/06/17.
 */

public class StudentListing extends Fragment {


    private static final String BRANCH_NAME = "BRANCH_NAME";
    private static String LISTING_TYPE="LISTING_TYPE";
    private ListView facultyList;
    private StudentListAdapter listingAdapter;
    private String listingType = "";
    // private Integer[] profileImag = {R.drawable.feed_person, R.drawable.img_feed_person, R.drawable.feed_person,R.drawable.img_img_feed, R.drawable.img_img_person,R.drawable.feed_person, R.drawable.img_img_person,R.drawable.img_img_person,R.drawable.feed_person,R.drawable.img_img_person_icon,R.drawable.img_img_person_icon,R.drawable.feed_person,R.drawable.feed_person, R.drawable.feed_person,R.drawable.img_img_person_icon,R.drawable.feed_person};
    private String branchName;
    private StudentModel studentModel;

    public static StudentListing newInstance(String listingType, String branchName) {
        Bundle args = new Bundle();
        StudentListing fragment = new StudentListing();
        fragment.setArguments(args);
        args.putString(BRANCH_NAME, branchName);
        args.putString(LISTING_TYPE,listingType);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.from(
                getActivity()).inflate(R.layout.faculy_list, container, false);
        HomeMainActivity.hideMoreButton();
        HomeMainActivity.setTitle("Students");
        facultyList = (ListView) view.findViewById(R.id.listLV);
        if (getArguments() != null) {
            branchName = getArguments().getString(BRANCH_NAME);
            this.listingType=getArguments().getString(LISTING_TYPE);
        }
        if (studentModel == null) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<StudentModel> shareModelCall = apiInterface.getStudentData();
            shareModelCall.enqueue(new Callback<StudentModel>() {
                @Override
                public void onResponse(Call<StudentModel> call, Response<StudentModel> response) {
                    studentModel = response.body();
                    showStudentList(studentModel);
                    //Toast.makeText(getActivity(), "Data length : " + studentModel.getData().toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<StudentModel> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        } else {
            showStudentList(studentModel);
        }
        return view;
    }

    private void showStudentList(StudentModel data) {
        if (data != null) {
            listingAdapter = new StudentListAdapter(getActivity(), data.getData());
            facultyList.setAdapter(listingAdapter);
        }
    }
}
