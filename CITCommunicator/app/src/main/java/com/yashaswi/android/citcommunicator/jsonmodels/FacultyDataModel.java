package com.yashaswi.android.citcommunicator.jsonmodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yashaswi on 14/06/17.
 */

public class FacultyDataModel {
    @SerializedName("name")
    private String name;

    @SerializedName("designation")
    private String designation;

    @SerializedName("department")
    private String dept;

    @SerializedName("experience")
    private int exp;

    @SerializedName("specialization")
    private String specialization;

    @SerializedName("email")
    private String email;

    @SerializedName("phone")
    private long phn;
    @SerializedName("photo")
    private int photo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getPhn() {
        return phn;
    }

    public void setPhn(long phn) {
        this.phn = phn;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "ClassPojo [name = " + name + ", designation = " + designation + ", dept = " + dept + ", exp = " + exp + ",  specialization= " + specialization + ", email = " + email + ", phn = " + phn + ", photo = " + photo + "]";
    }

}
