package com.yashaswi.android.citcommunicator.jsonmodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yashaswi on 14/06/17.
 */

public class FacultyModel {

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private FacultyDataModel[] data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public FacultyDataModel[] getData() {
        return data;
    }

    public void setData(FacultyDataModel[] data) {
        this.data = data;
    }

}
