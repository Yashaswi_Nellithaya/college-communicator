package com.yashaswi.android.citcommunicator.jsonmodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yashaswi on 14/06/17.
 */

public class StudentDataModel {

    @SerializedName("name")
    private String name;

    @SerializedName("department")
    private String dept;

    @SerializedName("experience")
    private int exp;

    @SerializedName("semester")
    private String sem;

    @SerializedName("marks")
    private int marks;

    @SerializedName("attendace")
    private int attendance;

    @SerializedName("photo")
    private int photo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public String getSem() {
        return sem;
    }

    public void setSem(String sem) {
        this.sem = sem;
    }

    public int getMarks() {
        return marks;
    }

    public void setMarks(int email) {
        this.marks = email;
    }

    public int getAttendance() {
        return attendance;
    }

    public void setAttendance(int attendance) {
        this.attendance = attendance;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }


}
