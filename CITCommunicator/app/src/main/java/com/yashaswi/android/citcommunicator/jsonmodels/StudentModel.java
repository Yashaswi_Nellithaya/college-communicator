package com.yashaswi.android.citcommunicator.jsonmodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yashaswi on 14/06/17.
 */

public class StudentModel {
    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private StudentDataModel[] data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public StudentDataModel[] getData() {
        return data;
    }

    public void setData(StudentDataModel[] data) {
        this.data = data;
    }
}
