package com.yashaswi.android.citcommunicator.jsonmodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yashaswi on 02/06/17.
 */

public class UserData {
    @SerializedName("username")
    private String username;

    @SerializedName("updated_at")
    private String updated_at;

    @SerializedName("_id")
    private String _id;

    @SerializedName("admin")
    private String admin;

    @SerializedName("name")
    private String name;
    @SerializedName("__v")
    private String __v;

    @SerializedName("created_at")
    private String created_at;
    @SerializedName("password")
    private String password;

    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getAdmin ()
    {
        return admin;
    }

    public void setAdmin (String admin)
    {
        this.admin = admin;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String get__v ()
    {
        return __v;
    }

    public void set__v (String __v)
    {
        this.__v = __v;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getPassword ()
    {
        return password;
    }

    public void setPassword (String password)
    {
        this.password = password;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [username = "+username+", updated_at = "+updated_at+", _id = "+_id+", admin = "+admin+", name = "+name+", __v = "+__v+", created_at = "+created_at+", password = "+password+"]";
    }
}
