package com.yashaswi.android.citcommunicator.jsonmodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yashaswi on 31/05/17.
 */

public class UserModel {
    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private UserData[] data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserData[] getData() {
        return data;
    }

    public void setData(UserData[] data) {
        this.data = data;
    }

}
